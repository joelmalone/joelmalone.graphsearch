﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.GraphSearch
{
    public class Edge<TState>
    {
        public Edge<TState> Parent { get; set; }
        public TState State { get; set; }
        public double Heuristic { get; set; }
        public double Cost { get; set; }
        public double TotalCost { get; set; }

        public override string ToString()
        {
            return string.Format(
                "Heuristic: {0:#,##0.0} Cost:{1:#,##0.0} TotalCost:{2:#,##0.0} State: {3}", 
                Heuristic, 
                Cost, 
                TotalCost, 
                State
                );
        }
    }

    public static class EdgeExtensions
    {

        public static IEnumerable<Edge<TState>> EnumerateUpParents<TState>(this Edge<TState> me)
        {
            var current = me;
            while (current != null)
            {
                yield return current;
                current = current.Parent;
            }
        }

    }

}
