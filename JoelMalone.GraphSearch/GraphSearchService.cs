﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JoelMalone.Logging;

namespace JoelMalone.GraphSearch
{

    public class GraphSearchService
    {

        private class EdgeHeuristicComparer<TState> : IComparer<Edge<TState>>
        {
            int IComparer<Edge<TState>>.Compare(Edge<TState> x, Edge<TState> y)
            {
                return (int)Math.Round(x.Heuristic - y.Heuristic, MidpointRounding.AwayFromZero);
            }
        }

        public IEnumerable<Edge<TState>> Solve<TState>(
            IEqualityComparer<TState> stateComparer,
            IEnumerable<TState> initialStates, 
            Func<TState, bool> successFunc,
            Func<Edge<TState>, double> optionalHeuristicFunc,
            Func<TState, TState, double> costFunc, 
            Func<TState, IEnumerable<TState>> nextStatesFunc
            )
        {
            var heuristicComparer = new EdgeHeuristicComparer<TState>();
            var untestedEdges = new List<Edge<TState>>();
            var testedEdges = new Dictionary<TState, Edge<TState>>(stateComparer);
            var bestResultTotalCost = double.MaxValue;

            var initialEdges = 
                from state in initialStates
                select new Edge<TState>
                {
                    Heuristic = 0,
                    Parent = null,
                    State = state,
                };
            untestedEdges.AddRange(initialEdges);

            while (untestedEdges.Count > 0)
            {
                if (optionalHeuristicFunc != null)
                {
                    // Recompute the heuristic for all untested edges so we can operate
                    //  on the best ones first.  Slow!  Maybe calc and sort on insert?
                    //  But, does the score change over time?
                    foreach (var i in untestedEdges)
                        i.Heuristic = optionalHeuristicFunc(i);
                    untestedEdges.Sort(heuristicComparer);
                }
                // Grab the best edge and remove it from the list; we sort on ascending
                //  fitness so the best is always the last, making for a fast removal.
                var lastIndex = untestedEdges.Count - 1;
                var thisEdge = untestedEdges[lastIndex];
                untestedEdges.RemoveAt(lastIndex);
                this.V("Testing edge {0}", thisEdge);

                // TODO: shouldn't we drop the edge if a solution already exists with 
                //  a better cost?  This block may need some rethinking.
                if (thisEdge.TotalCost < bestResultTotalCost && successFunc(thisEdge.State))
                {
                    bestResultTotalCost = thisEdge.TotalCost;
                    yield return thisEdge;

                    var solution = thisEdge.EnumerateUpParents().Reverse().ToList();
                    this.V("Returning solution with {0} edges:{1}", 
                        solution.Count,
                        thisEdge.EnumerateUpParents().Reverse().Aggregate("", (a, s) => a += "\n" + s)
                        );
                    // TODO: drop the edge?  Why would we keep looking down this path?  Need to
                    //  consider all outcomes for the rest of this iteration.
                }

                // Check if this state already has a path to it, i.e. have we
                //  arrived at this state already in our testing
                Edge<TState> existingStateEdge;
                if (testedEdges.TryGetValue(thisEdge.State, out existingStateEdge))
                {
                    if (thisEdge.TotalCost < existingStateEdge.TotalCost)
                    {
                        // We have found a better path to this state, so replace it
                        existingStateEdge.Parent = thisEdge.Parent;
                        existingStateEdge.Heuristic = thisEdge.Heuristic;   // Probably not necessary for tested nodes
                        existingStateEdge.Cost = thisEdge.Cost;
                        existingStateEdge.TotalCost = thisEdge.TotalCost;
                        // Note that we don't need to add any untested edges, as they
                        //  would have already been queued up when the replaced edge
                        //  was tested
                    }
                    else
                    {
                        // A better path exists to this state, so discard this one
                    }
                }
                else
                {
                    // This state does not exist, so add it and queue it for testing
                    testedEdges[thisEdge.State] = thisEdge;
                    // Queue up the next edges from this edge
                    foreach (var nextState in nextStatesFunc(thisEdge.State))
                    {
                        var cost = costFunc(thisEdge.State, nextState);

                        untestedEdges.Add(new Edge<TState>
                        {
                            Parent = thisEdge,
                            State = nextState,
                            Cost = cost,
                            TotalCost = thisEdge.TotalCost + cost,
                            // No need to set heurstic as it will be calculated next loop
                        });
                    }
                }
            }
        }

    }

}
